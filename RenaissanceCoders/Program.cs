﻿using System;

namespace RenaissanceCoders
{
    class Program
    {
        static void Main(string[] args)
        {
            Enemy generic = new Enemy();
            Enemy zombie = new Zombie();
            Enemy dragon = new Dragon("Nils");
            
            generic.Attack();
            zombie.Attack();
            dragon.Attack();
        }
    }
}
