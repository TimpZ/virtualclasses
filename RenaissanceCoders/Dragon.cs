using System;
using System.Collections.Generic;
using System.Text;

namespace RenaissanceCoders
{

    class Dragon : Enemy
    {
        public Dragon(string name)
        {
            this.name = name;
            this.damage = 999f;
        }
    }

}