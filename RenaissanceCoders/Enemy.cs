﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RenaissanceCoders
{
    interface IEnemy
    {
        string name { get; set; }
        float damage {get; set;}
        void Attack();
    }

    class Enemy : IEnemy
    {
        public string name {get; set;}
        public float damage {get;set;}

        public Enemy()
        {
            name = "Generic Enemy";
            damage = 10f;
        }
        public Enemy(string name, float damage)
        {
            this.name = name;
            this.damage = damage;
        }
        public virtual void Attack()
        {
            Console.WriteLine($"{name} attacked you for {damage} HP!");
        }
    }
}
