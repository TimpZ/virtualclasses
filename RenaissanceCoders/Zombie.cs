using System;
using System.Collections.Generic;
using System.Text;

namespace RenaissanceCoders
{

    class Zombie : Enemy
    {
        public override void Attack()
        {
            Console.WriteLine($"This is a Zombie attacking you for {damage} hitpoints");
        }
    }

}